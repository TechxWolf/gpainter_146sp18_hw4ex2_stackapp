
import java.util.LinkedList;


/**
 *
 * @author Griffin
 * Set up the stack class that uses a generic
 * @param <G>
 * 
 */
public class Stack<G> {
    
   private LinkedList<G> list  = new LinkedList<>();
   
    public void push(G s) {
        list.addFirst(s);
    }

    public G pop() {
        return list.removeFirst();
    }

    public G peek() {
        return list.getFirst();
    }

    public int size() {
        return list.size();
    }    
}//end class Stack<G>
    

